## sites

This is the home of all scrapers for SimplyNews

You can use those directly if you wish, but we recommend utilizing them
with a frontend like [simplynew_web](https://git.sr.ht/~metalune/simplynews_web)

### How to install
```sh
# Clone the git repository and navigate into it
git clone https://git.sr.ht/~metalune/simplynews_sites
cd simplynews_sites
# Install it
sudo python3 setup.py install
```

### Infos and TODOs
You can see our API Reference and TODO list [here](https://man.sr.ht/~metalune/simplynews/)
